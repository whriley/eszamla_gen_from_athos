from fpdf import FPDF
from fpdf.enums import XPos, YPos
from PyPDF2 import PdfReader, PdfWriter
from PyPDF2.generic import AnnotationBuilder
from tqdm import tqdm
import csv
import os
import argparse
import sys
import logging
import subprocess
import shutil
import pickle
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from io import BytesIO

####################
# Mappák beállítása
####################    
path = os.getcwd()
print("Current Directory", path)

# Szükséges mappák
elFolderPath = path+"\\EL\\"    
TemplateFolderPath = path+"\\template\\"

csvFile = elFolderPath+"mszam2.txt"

# Létrehozza ha nem létezik    
TempFolderPath = path+"\\temp\\"
ResultFolderPath = path+"\\e-szla\\"
GhostscriptPATH = path+"\\gs\\bin\\gswin64c.exe"

##########################################################
# CSV file Read #
##########################################################
def csvRead(filename):
    csvInputResult = []
    with open(filename, encoding='UTF-8') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='|')
        line_count = 0
        for row in csv_reader:
            if(row.__len__() > 0):
                csvInputResult.append(row)
    return csvInputResult


##########################################################
# Számla első oldal # 
##########################################################
def szamlaOldal_1(invoice, TempFolderPath, TemplateFolderPath, invoicesStats):    
    ##########################################################
    ######### PDF beállítások ######### 
    pdf = FPDF(orientation='P', unit='mm', format='A4')
    pdf.set_margins(1,1,1)
    # pdf.add_font('timesUni', '', r'MTIMES.ttf', uni=True) # Deprecated WARNING
    pdf.add_font('timesUni', '', r'MTIMES.ttf')
    pdf.add_font('timesUniB', '', r'MTIMESB.ttf')
    pdf.add_font('arialUni', '', r'ArialUnicodeMS.ttf')
    pdf.add_font('arialUniB', '', r'ArialUnicodeBold.ttf')
    
    ##########################################################
    ######### Lap hozzáadása a PDFhez #######
    pdf.set_y(0)
    pdf.set_x(0)
    pdf.add_page()
    col_width = pdf.w / 3.5
    row_height = pdf.font_size
    pdf = vizTavhoTipusVizsgaloRezsiFeltoltes(invoice, pdf)
    
    ##########################################################
    ######### Nyomdai csikozások ##########
    x1_pos = 3
    x2_pos = 15
    pdf.set_line_width(0.5)
    pdf.set_draw_color(r=0, g=0, b=0)
    pdf.line(x1=x1_pos, y1=50, x2=x2_pos, y2=50)
    pdf.line(x1=x1_pos, y1=57, x2=x2_pos, y2=57)
    pdf.line(x1=x1_pos, y1=60, x2=x2_pos, y2=60)

    ##########################################################
    ## Jobb felső rész ##
    pdf.set_font('timesUniB', '', 10)
    footer1_startx = 124
    footer1_starty = 6
    pdf.text(footer1_startx, footer1_starty, invoice[27])
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size)
    pdf.text(footer1_startx, footer1_starty, "1 sz. eredeti példány")
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size)
    pdf.text(footer1_startx, footer1_starty, invoice[28])
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size)
    pdf.text(footer1_startx, footer1_starty, "Számla sorszáma:   "+invoice[0])
    
    ##########################################################
    ### Címzési rész
    # Függvény a szöveg sorokra bontásához, és a maximális karakterek kezeléséhez
    def split_text_by_length(text, max_length, max_total_length):
        # Ha a teljes szöveg hosszabb, mint a max_total_length, akkor levágjuk
        if len(text) > max_total_length:
            text = text[:max_total_length]
        lines = []
        while len(text) > max_length:
            lines.append(text[:max_length])  # első max_length karakter
            text = text[max_length:]  # a maradék szöveg
        if len(text) > 0:
            lines.append(text)  # ha maradt még szöveg
        return lines

    pdf.set_font('arialUniB', '', 10)
    # Cimzes_szöveg - nev
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size*3)
    max_total_length = 120  # maximális karakter hosszúságra (utana csonkitas)
    max_line_length = 40  # maximális sor hosszúságra
    
    lines = split_text_by_length(invoice[6], max_line_length, max_total_length)  # "max line length" hosszu sorokra bontas
    for line in lines:
        pdf.text(footer1_startx, footer1_starty, line)  # szöveg kiírása minden sorra
        footer1_starty += pdf.font_size  # sorok közötti távolság növelése
    lines = []
    '''
    # pdf.text(footer1_startx, footer1_starty, invoice[6]) # kez_nev+nevkieg
    # pdf.text(footer1_startx, footer1_starty, invoice[24]) # nevkieg (ügyfél)
    '''
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size*1.5)
    pdf.text(footer1_startx, footer1_starty, invoice[7].lstrip())
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size*1.5)
    pdf.text(footer1_startx, footer1_starty, invoice[8].lstrip())
    footer1_starty = newLineSpacingYCalc(footer1_starty, pdf.font_size*1.5)
    pdf.text(footer1_startx, footer1_starty, invoice[9].lstrip())
    pdf.set_font('arialUniB', '', 8)
    pdf.text(footer1_startx+55, footer1_starty, str(invoice[30]).zfill(5)+"_"+str(invoice[31]).zfill(5))
    
    row_height = pdf.font_size
    line_height = pdf.font_size * 2.5

    ##########################################################
    #### Felhasználói adatok
    
    pdf.set_font('timesUni', '', 9.5)
    felhasznaloiData_Startx = 95
    felhasznaloiData_Starty = 64
    pdf.set_y(felhasznaloiData_Starty)
    pdf.set_x(felhasznaloiData_Startx)
    line_height = pdf.font_size * 1.38
    col_width = 46 # distribute content evenly
    felhasznaloiData = (
        ("Felhasználó azonosító száma:", invoice[10]),
        ("Felhasználó neve:", invoice[11]+" "+invoice[24]),
        ("Felhasználó címe:", invoice[12]+"\n"+invoice[13].lstrip()),
        ("Felhasználási hely címe:", invoice[14]+"\n"+invoice[15].lstrip()),
        ("Hőközponti azonosító:", invoice[16].lstrip()),
    )
    if(felhasznaloiData[4][1] == ""):
        print("Hőközponti azonosító üres string! Jelöletlenül: "+felhasznaloiData[4][1])
        felhasznaloiData[4][1] = "Nincs azonosító"
    for row in felhasznaloiData:
        x = 1
        row_height_lines = 1
        lines_in_row = []
        for cell in row:
            output = pdf.multi_cell(col_width * 1.15, line_height, cell, border=0, dry_run=True, output="LINES")
            lines_in_row.append(len(output))
            if len(output) > row_height_lines:
                row_height_lines = len(output)

        for tlines, datum in zip(lines_in_row, row):
            # here you can hack-in the
            text =datum.rstrip('\n') + (1 + row_height_lines - tlines) * '\n'
            pdf.multi_cell(col_width * 1.15, line_height, text, border=0, new_x=XPos.RIGHT, new_y=YPos.TOP)
        pdf.ln(row_height_lines * line_height)

        pdf.set_x(felhasznaloiData_Startx)

    pdf.set_line_width(0.1)
    pdf.rect(felhasznaloiData_Startx-3, felhasznaloiData_Starty-3, 110, 47, round_corners=False, style="D") # 110,46

    ##########################################################
    # Szolgáltatói információk (Bal felso) #
    
    TszolData_Startx = 20
    TszolData_Starty = 32
    pdf.set_x(TszolData_Startx)
    pdf.set_y(TszolData_Starty)
    line_height = pdf.font_size * 1.3
    col_width = pdf.epw / 2 # distribute content evenly
    pdf.set_font('timesUniB', '', 9.5)

    Left_cell_width=30
    NewLineSpacing = 1.2
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Szolgáltató neve:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "T-Szol Tatabányai Szolgáltató Zrt.")
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Címe:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "2800 Tatabánya, Győri út 23.")
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Adószáma:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "11183934-2-11")
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Bankszámlaszáma:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "10300002-40005102-00003285")

    Left_cell_width = 20
    NewLineSpacing = 1.5
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty+5, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Ügyfélszolgálat elérhetőségei:")
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Tatabánya, Fő tér 8/A")

    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Telefon:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "(34) 600-700")
    TszolData_Starty = newLineSpacingYCalc(TszolData_Starty, pdf.font_size*NewLineSpacing)
    pdf.text(TszolData_Startx, TszolData_Starty, "Email:")
    pdf.text(TszolData_Startx+Left_cell_width, TszolData_Starty, "tavho.ugyfelszolgalat@tszol.hu")
   
    ##########################################################
    # Fizetési adatok lila (RezsiTagElső) #
    ##########################################################
    RezsiTagElso_Startx = 15
    RezsiTagElso_Starty = 84 
    pdf.set_x(RezsiTagElso_Startx)
    pdf.set_y(RezsiTagElso_Starty)
    
    pdf.set_font('timesUniB', '', 11)
    line_height = pdf.font_size * 1.51
    col_width = 37.5 # distribute content evenly
    
    if(invoicesStats['maxPrice'] < int(invoice[17])):
        invoicesStats['invoiceNumber'] = invoice[0]
        invoicesStats['maxPrice'] = int(invoice[17])

    RezsiTagElsoData = (
        ("Elszámolási időszak:", invoice[5]),
        ("Fizetendő összeg:", invoice[17]+" Ft"),
        ("Fizetési határidő:", invoice[18]),
    )

    for row in RezsiTagElsoData:
        x = 0        
        for cell in row:
            if(x%2 == 0):             
                pdf.cell(w=13.5, h=line_height, text="", align='L', border=0) # Fake Cell, mert a pdf elejétől kezdi nem fogad el kezdő kordinátát (ha látni akarod border=1)
                pdf.cell(w=col_width, h=line_height, text=cell, align='L', border=0)
                x=x+1
            else:        
                #pdf.cell(w=col_width, h=line_height, text=cell, align='R', border=1)
                pdf.cell(w=col_width, h=line_height, text=cell, align='R', border=0)
                # pdf.set_y(RezsiTagElso_Starty)
                x=x+1
                pdf.ln(line_height)
    
    pdfOutputName = TempFolderPath+invoice[0]+"_page1.pdf"
    pdf.output(pdfOutputName)
    return pdfOutputName
##########################################################

##########################################################
# NAGY LILA #
##########################################################
def rezsiLilaNagy(invoice, pdf):
##########################################################
  
    RezsiTagMasodik_Startx = 15
    RezsiTagMasodik_Starty = 116
    # pdf.set_xy(30,80)    
    pdf.set_x(RezsiTagMasodik_Startx)
    pdf.set_y(RezsiTagMasodik_Starty)
    
    pdf.set_font('timesUniB', '', 11)
    line_height = pdf.font_size * 1.6
    col_widthDef = 154
    col_widthSummary = 26
    RezsiElottiDij = int(invoice[17])+int(invoice[23]) # "A rezsidíj csökkentése nélküli ár kiszámítása
    RezsiTagElsoData = (
        ("A rezsidíj csökkentése nélkül Önt ebben az elszámolási időszakban \na következő fizetési kötelezettség terhelte volna:", str(RezsiElottiDij)+" Ft"),
        ("A rezsidíj csökkentésének eredményeképpen az Ön megtakarítása a távhőszolgáltatásból:", ""),
        ("- ebben az elszámolási időszakban:", invoice[19]+" Ft"),
        ("- 2013 január 1-je óta összesen:", invoice[20]+" Ft"),
        ("A rezsidíj csökkentésének eredményeképpen az Ön megtakarítása a víziközmű-szolgáltatásból", ""),
        ("- ebben az elszámolási időszakban:", invoice[21]+" Ft"),
        ("- 2013 január 1-je óta összesen:", invoice[22]+" Ft"),
        ("Ebben az elszámolási időszakban a megtakarítása összesen:", invoice[23]+" Ft"),
        ("(Ezen összegek az Ön számlájából előzetesen levonásra kerültek.)", ""),
    )

    for row in RezsiTagElsoData:
        x = 0        
        for cell in row:
            if(x%2 == 0):             
                lines = len(cell.split("\n"))              
                pdf.cell(w=13.5, h=line_height, text="", align='L', border=0) # Fake Cell, mert a pdf elejétől kezdi nem fogad el kezdő kordinátát (ha látni akarod border=1)
                y=0
                for line in cell.split("\n"):  
                    if(y == 1):
                        pdf.cell(w=col_widthSummary, h=line_height, text="", align='L', border=0)
                        pdf.ln(line_height)
                        pdf.cell(w=13.5, h=line_height, text="", align='L', border=0)
                    pdf.cell(w=col_widthDef, h=line_height, text=line, align='L', border=0)
                    y=y+1
                x=x+1
            else:        
                #pdf.cell(w=col_width, h=line_height, txt=cell, align='R', border=1)
                pdf.cell(w=col_widthSummary, h=line_height, text=cell, align='R', border=0)
                # pdf.set_y(RezsiTagElso_Starty)
                x=x+1
                pdf.ln(line_height)
            
    return pdf
##########################################################

##########################################################
# Nincs rezsi esetén, rezsi adat üresre állítása
##########################################################
def NincsrezsiLilaKicsi(invoice, pdf):
    RezsiTagElsoData = ""
    return pdf

##########################################################


##########################################################
# Van rezsi kicsi (nincs víz esetén)
##########################################################
def rezsiLilaKicsi(invoice, pdf):

    RezsiTagMasodik_Startx = 15
    RezsiTagMasodik_Starty = 131
    # pdf.set_xy(30,80)    
    pdf.set_x(RezsiTagMasodik_Startx)
    pdf.set_y(RezsiTagMasodik_Starty)
    
    pdf.set_font('timesUniB', '', 11)
    line_height = pdf.font_size * 1.2
    col_widthDef = 154
    col_widthSummary = 26
    RezsiElottiDij = int(invoice[17])+int(invoice[23]) # "A rezsidíj csökkentése nélküli ár kiszámítása
    RezsiTagElsoData = (
        ("A rezsidíj csökkentése nélkül Önt ebben az elszámolási időszakban \na következő fizetési kötelezettség terhelte volna:", str(RezsiElottiDij)+" Ft"),
        ("A rezsidíj csökkentésének eredményeképpen an Ön megtakarítása a távhőszolgáltatásból:", ""),
        ("- ebben az elszámolási időszakban:", invoice[19]+" Ft"),
        ("- 2013 január 1-je óta összesen:", invoice[20]+" Ft"),
        ("Ebben az elszámolási időszakban a megtakarítása összesen:", invoice[23]+" Ft"),
        ("(Ezen összegek az Ön számlájából előzetesen levonásra kerültek.)", ""),
    )

    for row in RezsiTagElsoData:
        x = 0        
        for cell in row:
            if(x%2 == 0):             
                lines = len(cell.split("\n"))              
                pdf.cell(w=13.5, h=line_height, text="", align='L', border=0) # Fake Cell, mert a pdf elejétől kezdi nem fogad el kezdő kordinátát (ha látni akarod border=1)
                y=0
                for line in cell.split("\n"):  
                    if(y == 1):
                        pdf.cell(w=col_widthSummary, h=line_height, text="", align='L', border=0)
                        pdf.ln(line_height)
                        pdf.cell(w=13.5, h=line_height, text="", align='L', border=0)
                    pdf.cell(w=col_widthDef, h=line_height, text=line, align='L', border=0)
                    y=y+1
                x=x+1
            else:        
                #pdf.cell(w=col_width, h=line_height, txt=cell, align='R', border=1)
                pdf.cell(w=col_widthSummary, h=line_height, text=cell, align='R', border=0)
                # pdf.set_y(RezsiTagElso_Starty)
                x=x+1
                pdf.ln(line_height)                
    return pdf
##########################################################


##########################################################
# rezsi/víz/csekk vizsgálat
##########################################################

def vizTavhoTipusVizsgaloRezsiFeltoltes(invoice, pdf):
    # Loggolási adatok
    logger.info("invoice[0] (Szamlaszam) == "+str(invoice[0]))
    logger.info("invoice[29] (Rezsicsokkentett) == "+str(invoice[29]))
    logger.info("invoice[3] ([Vizes] vagy [nem vizes (T)]) == "+str(invoice[3]))
    logger.info("invoice[2] (Csekkes) == "+str(invoice[2]))
    #################################################################################
    if(invoice[29] == 'I'): # Van-e rezsi
        if(invoice[3] == 'V'): # Vizes
            imagePath = TemplateFolderPath+"tszol_nagy.png"
            # Ez a számlamásolat készítésekor érdekes, ha úgy használjuk, és csekkes az ügyfél akkor lecseréljük a templatet csekkes pdf-re [szamla2oldal fv. is van egy ilyen vizsgálat!!!]       
            if(invoice[2] == 'C'):        
                imagePath = TemplateFolderPath+"tszol_csekkes_nagy.jpg"
            pdf.image(imagePath,x=0,y=0,w=210)
            pdf = rezsiLilaNagy(invoice, pdf)
        else: # if(invoice[3] == 'T'): <--- erre számítanánk azaz T=Fűtéses (távhő)
            imagePath = TemplateFolderPath+"tszol_kicsi.png"
            if(invoice[2] == 'C'):        
                imagePath = TemplateFolderPath+"tszol_csekkes_kicsi.jpg"
            pdf.image(imagePath,x=0,y=0,w=210)
            pdf = rezsiLilaKicsi(invoice, pdf)
    else:
        imagePath = TemplateFolderPath+"tszol_kicsi.png"
        # Ez a számlamásolat készítésekor érdekes, ha úgy használjuk, és csekkes az ügyfél akkor lecseréljük a templatet csekkes pdf-re [szamla2oldal fv. is van egy ilyen vizsgálat!!!]       
        if(invoice[2] == 'C'):        
            imagePath = TemplateFolderPath+"tszol_csekkes_kicsi.jpg"
        pdf.image(imagePath,x=0,y=0,w=210)                     
        pdf = NincsrezsiLilaKicsi(invoice, pdf)
    return pdf


##########################################################
# Második oldal #
##########################################################

def szamlaOldal_2(invoice, TempFolderPath, pdf2OldalMinta):
    # Oldalszám formázása
    pageNum = invoice[28]
    page_number_text = f"2/{pageNum[2:3]}. oldal"

    # Beolvassuk az eredeti PDF-et a méret miatt
    existing_pdf = PdfReader(pdf2OldalMinta)
    page = existing_pdf.pages[0]
    page_width = float(page.mediabox.width)
    page_height = float(page.mediabox.height)

    # Új PDF réteg létrehozása szöveggel
    packet = BytesIO()
    can = canvas.Canvas(packet, pagesize=(page_width, page_height))
    can.setFont("Helvetica-Bold", 12)
    
    # Szöveg jobb felső sarokba helyezése (10px margóval)
    can.drawString(page_width - 80, page_height - 25, page_number_text)
    can.save()

    # Szöveges PDF beolvasása
    packet.seek(0)
    new_pdf = PdfReader(packet)

    # Összeolvasztás
    output = PdfWriter()
    page.merge_page(new_pdf.pages[0])
    output.add_page(page)

    # Mentés
    pdfOutputFileName = TempFolderPath + invoice[0] + "_page2.pdf"
    with open(pdfOutputFileName, "wb") as fp:
        output.write(fp)

    return pdfOutputFileName
##########################################################

##########################################################
##### Összefűzés (Merge) ######   
def szamlaOldal_3(args, invoice, pdfForMergeList, ResultFolderPath):
    
    merger = PdfWriter()
    
    for pdf in pdfForMergeList:
        if args.compress == 1:
            if (invoice[2] != 'C'): 
                compressPDF(pdf)
            if is_last_page_empty(pdf):
                remove_last_empty_page(pdf)
            # Ellenőrzi, hogy az utolsó oldal üres-e, és ha igen, törli
            
        merger.append(pdf)        

    pdfOutputFileName = ResultFolderPath+invoice[0]+".pdf"
    merger.write(pdfOutputFileName)
    merger.close()
    if args.compress == 1:      
       compressPDF(pdfOutputFileName)
    return pdfOutputFileName
##########################################################


def is_last_page_empty(pdf_file):
    # Ellenőrzi, hogy a PDF utolsó oldala üres-e
    with open(pdf_file, 'rb') as f:
        pdf = PdfReader(f)
        if len(pdf.pages) > 0:
            last_page = pdf.pages[-1]
            if last_page.extract_text().strip() == '':
                return True
    return False

def remove_last_empty_page(pdf_file):
    # Törli a PDF utolsó üres oldalát
    with open(pdf_file, 'rb') as f:
        pdf = PdfReader(f)
        if len(pdf.pages) > 0:
            writer = PdfWriter()
            for page_num in range(len(pdf.pages) - 1):
                writer.add_page(pdf.pages[page_num])
            with open(pdf_file, 'wb') as output_file:
                writer.write(output_file)




def newLineSpacingYCalc(start, increment):
    return start+increment

# PyPdf2 alap tömörítője
##########################################################
'''
def compressPDF(pdfOutputFileName):
    reader = PdfReader(pdfOutputFileName)
    writer = PdfWriter()

    for page in reader.pages:
        page.compress_content_streams()  # This is CPU intensive!
        writer.add_page(page)

    with open(pdfOutputFileName, "wb") as f:
        writer.write(f)
'''


def copy_file(source_file, destination_file):
    try:
        # Fájl másolása felülírással
        shutil.copyfile(source_file, destination_file)
        logger.info(f'A(z) {source_file} fájl másolása sikeresen megtörtént a(z) {destination_file} fájlba.')
    except FileNotFoundError:
        logger.error(f'Hiba: A(z) {source_file} fájl nem található.')
        print(f'Hiba: A(z) {source_file} fájl nem található.')
    except PermissionError:
        logger.error(f'Hiba: Hozzáférés megtagadva a(z) {destination_file} fájlhoz.')
        print(f'Hiba: Hozzáférés megtagadva a(z) {destination_file} fájlhoz.')
    except Exception as e:
        logger.error(f'Hiba történt a fájl másolása közben: {e}')
        print(f'Hiba történt a fájl másolása közben: {e}')


def compressPDF(pdfOutputFileName):
    # Eredeti PDF fájl méretének lekérdezése
    original_size = os.path.getsize(pdfOutputFileName)

    # Kimeneti tömörített PDF fájl elérési útja
    output_pdf_path = pdfOutputFileName+"1"

    # Ghostscript parancs összeállítása
    '''
    gs_command = [
        GhostscriptPATH,  # Ghostscript parancs
        "-sDEVICE=pdfwrite",  # PDF írás eszköz kiválasztása
        "-dPDFSETTINGS=/screen",  # Tömörítési beállítások (képernyőre optimalizált)
        "-dNOPAUSE", "-dQUIET", "-dBATCH",  # Egyéb paraméterek a folyamat automatizálásához
        f"-sOutputFile={output_pdf_path}",  # Kimeneti tömörített PDF fájl neve
        pdfOutputFileName  # Bemeneti PDF fájl neve
    ]
    '''

    gs_command = [
        GhostscriptPATH,  # Ghostscript parancs
        "-sDEVICE=pdfwrite",  # PDF írás eszköz kiválasztása
        "-dPDFSETTINGS=/ebook",  # Tömörítési beállítások 
        "-dNOPAUSE", "-dQUIET", "-dBATCH",  # Egyéb paraméterek a folyamat automatizálásához
        f"-sOutputFile={output_pdf_path}",  # Kimeneti tömörített PDF fájl neve
        pdfOutputFileName  # Bemeneti PDF fájl neve
    ]

    # Ghostscript parancs futtatása a subprocess modul segítségével
    try:
        subprocess.run(gs_command, check=True)
      
        # Tömörített PDF fájl méretének lekérdezése
        compressed_size = os.path.getsize(output_pdf_path)

        # Tömörítési arány százalékos értéke
        compression_ratio = (compressed_size / original_size) * 100
        logger.info(f"Tömörített PDF eredménye: {output_pdf_path} - {compression_ratio:.2f}%")
        copy_file(output_pdf_path, pdfOutputFileName)
        if os.path.exists(output_pdf_path):
            os.remove(output_pdf_path)
        else:
            print("The file does not exist: "+output_pdf_path)
            logger.error("The file does not exist: "+output_pdf_path)
        return pdfOutputFileName  # Visszaadjuk a tömörített PDF fájl elérési útját

    except subprocess.CalledProcessError as e:
        print(f"Hiba történt a PDF tömörítése közben: {e}")
        logger.error(f"Hiba történt a PDF tömörítése közben: {e}")
        return None


##########################################################
# Futás előtti ellenőrzés #
# Végig nézzük mind, hogy ne egyesével kapjuk meg a hibákat
##########################################################
def checkList(elFolderPath, TemplateFolderPath, TempFolderPath, ResultFolderPath, csvFile):
    checkRes = True
    if not os.path.isfile(csvFile):
        logger.error("Nem található a "+csvFile+"!")
        print("Nem található a "+csvFile+"!")
        checkRes = False
        sys.exit(1)  
    if not os.path.exists(elFolderPath):
        logger.error("Nem található az "+elFolderPath+" mappa, a számlarészletező oldalak nem érhetők el!")
        print("Nem található az "+elFolderPath+" mappa, a számlarészletező oldalak nem érhetők el!")        
        checkRes = False
        sys.exit(1)  
    if not os.path.exists(TemplateFolderPath):
        logger.error("Nem található az "+TemplateFolderPath+" mappa, a mintaoldal mappája nem érhető el!")
        print("Nem található az "+TemplateFolderPath+" mappa, a mintaoldal mappája nem érhető el!")
        checkRes = False
        sys.exit(1)  
    if not os.path.exists(TempFolderPath):
        os.makedirs(TempFolderPath)
        if(os.path.exists(TempFolderPath)):
            print(TempFolderPath+" created!")
            logger.info(TempFolderPath+" created!")
        else:
            print(TempFolderPath+" create error!")
            logger.error(TempFolderPath+" created error!")
            checkRes = False
            sys.exit(1)  
    if not os.path.exists(ResultFolderPath):
        os.makedirs(ResultFolderPath)
        if(os.path.exists(ResultFolderPath)):
            print(ResultFolderPath+" created!")
            logger.info(ResultFolderPath+" created!")
        else:
            print(ResultFolderPath+" create error!")
            logger.Error(ResultFolderPath+" created error!")
            checkRes = False
            sys.exit(1)            
    return checkRes

def argsCheck(args, invoiceList):
    
    ################################
    # Felfele írj inkább további feltételt.
    # Lényeges a sorrend az elején akik függetlenek az invoiceListtől és egymástól, 
    # mert megvágva vagy változatlanul megy vissza az invoiceList.
    ################################
    if args.rownum is not None:
            print("Compress PDF: "+args.compress)
            logger.info("Compress PDF: "+args.compress)

    if args.rownum is not None:
        if args.rownum == "get":
            print("Added rownum arg: "+args.rownum)
            logger.info("Added rownum arg: "+args.rownum)
            print("Eszamlak szama: "+str(len(invoiceList)))                
            logger.info("Eszamlak szama: "+str(len(invoiceList)))
            sys.exit(0)
        else:
            print("Added rownum arg: "+args.rownum)
            logger.info("Added rownum arg: "+args.rownum)
            print("Nem megfelelo ertek van megadva a rownum parameterhez! Program leall!")
            logger.critical("Nem megfelelo ertek van megadva a rownum parameterhez! Program leall!")
            sys.exit(1)

    if args.addpdflist is not None:
        args.addpdflist = addpdfListChecker(args.addpdflist)
        print(args.addpdflist)
        logger.info(args.addpdflist)

    startLen = len(invoiceList)
    if((args.startrow is not None) and (args.endrow is not None)):
        if (0 < args.startrow <= args.endrow <= len(invoiceList)):
            print("Added Startrow arg: "+str(args.startrow))
            logger.info("Added Startrow arg: "+str(args.startrow))
            invoiceList = invoiceList[args.startrow-1:args.endrow-1] # Ha startrow argumentummal vágjuk a számlák listáját. (InputCSV)
            print("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))
            logger.info("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))        
            return invoiceList
        else:
            print("Nem megfelelo a startrow argumentum, nincs tartomanyon belul! 0 < args.startrow <= args.startrow --> startrow: "+str(args.startrow)+" ---> endrow: "+str(args.endrow))
            logger.ERROR("Nem megfelelo a startrow argumentum, nincs tartomanyon belul! 0 < args.startrow <= args.startrow --> startrow: "+str(args.startrow)+" ---> endrow: "+str(args.endrow))
            sys.exit(1)

    if args.startrow is not None:
        if 0 < args.startrow <= len(invoiceList):
            # A tomb szamozas 0-tol kezdodik ezert a user szamabol 1-et erdemes kivonni      
            print("Added startrow arg: "+str(args.startrow))
            logger.info("Added startrow arg: "+str(args.startrow))
            invoiceList = invoiceList[args.startrow-1:] # Ha startrow argumentummal vágjuk a számlák listájának elejét. (InputCSV)
            print("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))
            logger.info("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))   
            return invoiceList
        else:
            print("Nem megfelelo a startrow argumentum, nincs tartomanyon belul! startrow: "+str(args.startrow))
            logger.ERROR("Nem megfelelo a startrow argumentum, nincs tartomanyon belul! startrow: "+str(args.startrow))
            sys.exit(1)
 
    if args.endrow is not None:
        if args.endrow <= len(invoiceList):
            print("Added endrow arg: "+str(args.endrow))
            logger.info("Added endrow arg: "+str(args.endrow))
            invoiceList = invoiceList[:args.endrow-1] # Ha endrow argumentummal vágjuk a számlák listáját. (InputCSV)
            print("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))
            logger.info("Megadott intervallumok beallitasa utan beallitott szamlak szama: "+str(len(invoiceList)))
            return invoiceList
        else:
            print("Nem megfelelo a endrow argumentum, nincs tartomanyon belul! endrow: "+str(args.endrow))
            logger.ERROR("Nem megfelelo a endrow argumentum, nincs tartomanyon belul! endrow: "+str(args.endrow))
            sys.exit(1)
    return invoiceList

# Megadott argsként pdf fájlokat ellenőrzi
def addpdfListChecker(argplusPdf):
    pathFolder = os.getcwd()
    PathMergedArgplusPdf = []
    for pdf in argplusPdf:
        if not os.path.isfile(pathFolder+pdf):
            logger.error("Nem található az argumentumkent megadott "+os.path.abspath(pathFolder+pdf)+" fájl!")
            print("Nem található az argumentumkent megadott "+os.path.abspath(pathFolder+pdf)+" fájl!")        
            checkRes = False
            sys.exit(1)
        else:
            print("A megadott "+pdf+" elerheto!")
            logger.info("A megadott "+pdf+" elerheto!")
            PathMergedArgplusPdf.append(pathFolder+pdf)
    return PathMergedArgplusPdf

def createLogger():
    # Create and configure logger
    logging.basicConfig(filename="eszamla_gen.log",
                    format='%(asctime)s %(message)s',
                    filemode='a')
    # Creating an object
    logger = logging.getLogger()
    # Setting the threshold of logger to info
    logger.setLevel(logging.INFO)
    return logger

def mainRun():
    ######################
    # Csak így jó a global, így minden függvény eléri 
    global logger 
    logger = createLogger()
    #######################
    parser = argparse.ArgumentParser()
    parser.add_argument("--startrow", type=int, required=False, help='Megadott sortol kezdi a feldolgozast! (int)')
    parser.add_argument("--endrow", type=int, required=False, help='Megadott sorig (a megadottat mar nem) vegzi a feldolgozast! (int)')
    parser.add_argument("--rownum", type=str, required=False, help='Futas nelkul megadja a darabszamot, hasznalata --rownum "get"!')
    parser.add_argument('--addpdflist', nargs='*', required=False, help='Plusz pdf oldalak hozzafuzese a szamlahoz, sorrendben fuzi. Utvonal megadasa, relativ eleresi utvonallal adhato, az alabbi formaban: "\\mappa\\fajl.pdf"  pl.: --addpdflist "\\template\\newAddPdfName.pdf" "\\template\\newAddPdfName2.pdf"')
    parser.add_argument("--compress", type=int, required=False, default=1, help='Alapertelmezetten tomoritett pdf generalas. 0=nem, 1=igen')
    args = parser.parse_args()
    
    checkListRes = checkList(elFolderPath, TemplateFolderPath, TempFolderPath, ResultFolderPath, csvFile)
    if (checkListRes):
        logger.info("elFolderPath = "+elFolderPath)
        logger.info("TemplateFolderPath = "+TemplateFolderPath)
        logger.info("TempFolderPath = "+TempFolderPath)
        print("elFolderPath = "+elFolderPath)
        print("TemplateFolderPath = "+TemplateFolderPath)
        print("TempFolderPath = "+TempFolderPath)
        parent = os.path.dirname(elFolderPath)
        print("Parent directory", parent)

        invoicesStats = {
            'invoiceNumber': "",
            'maxPrice': 0
            }

        ####################
        # Szamlak kezdés
        ####################
        invoiceList = csvRead(csvFile)
        invoiceList = argsCheck(args, invoiceList) # Argumentumok fuggvenyeben modositjuk az invoiceList tombot
        eszamlaDB = len(invoiceList)
        i = eszamlaDB
        for invoice in tqdm(invoiceList):
            pdf_page1 = szamlaOldal_1(invoice, TempFolderPath, TemplateFolderPath, invoicesStats)
            szamlaPage2TemplatePath = TemplateFolderPath+"szamla_template_page2.pdf"
            if(invoice[2] == 'C'):
                szamlaPage2TemplatePath = TemplateFolderPath+"tszol_csekkes_page2.pdf"
            pdf_page2 = szamlaOldal_2(invoice, TempFolderPath, szamlaPage2TemplatePath)
            pdfForMergeList = [pdf_page1,pdf_page2, elFolderPath+"\\"+invoice[0]+".pdf"]
            if args.addpdflist is not None:               
                pdfForMergeList.extend(args.addpdflist)
            pdfResult = szamlaOldal_3(args, invoice, pdfForMergeList, ResultFolderPath)
            logger.info(str(i)+"/"+str(eszamlaDB)+" szamla: "+pdfResult)
            logger.info(pdfResult)        
            i = i-1
        print("Statistic:")
        print("Max Price: "+ str(invoicesStats['maxPrice']) + "\tInvoiceNumber: "+ str(invoicesStats['invoiceNumber']))
        logger.info("Max Price: "+ str(invoicesStats['maxPrice']) + "\tInvoiceNumber: "+ str(invoicesStats['invoiceNumber']))
    else:
        print("CheckList hiba! Ellenőrizd a listát! A program leáll!")
        sys.exit(1)

if __name__ == '__main__':
    mainRun()